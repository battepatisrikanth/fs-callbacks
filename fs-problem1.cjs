const fs = require('fs');
const path = require('path');

function createDirectory(directoryPath, numberOfFiles) {
    fs.mkdir(directoryPath, (error) => {
        if (error) {
            console.error(error);
        } else {
            console.log("Directory Created Successfully");
            createRandomFiles(directoryPath, numberOfFiles);
        }
    });

    function createRandomFiles(directoryPath, numberOfFiles) {
    
        let pathArray = [];
        for (let index = 1; index <= numberOfFiles; index++) {
            const fileName = `file_${index}.json`;
            const filePath = path.join(directoryPath, fileName);

            fs.writeFile(filePath, '', (error) => {
                if (error) {
                    console.error(error);
                } else {
                    pathArray.push(filePath);
                    console.log("A file has been created successfully");

                    if (pathArray.length == numberOfFiles) {
                        deleteFiles(null, pathArray)
                    }
                }
            });
            //console.log(pathArray.length);

        }

        function deleteFiles(error, pathArray) {
            if (error) {
                console.error(error);
            }
            else {
                for (let index = 0; index < pathArray.length; index++) {
                    fs.unlink(pathArray[index], function (error) {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log("File deleted");
                        }
                    })
                }
            }
        }
        //console.log(pathArray);
    }
}

module.exports = createDirectory;